/**
 * Created by as on 27/11/19.
 */

$(function () {
    $('#selectrowperpage').on("change", function () {
        var value_selectshortbyitem = $('#selectshortbyitem').val();
        var value_selectshortby = $('#selectshortby').val();
        var value_selectrowperpage = $('#selectrowperpage').val();
        var value_search = $('#searchboxinput').val();
        var value_offset = 1;
        var value = {
            editreq: 0,
            shortbyitem: value_selectshortbyitem,
            shortby: value_selectshortby,
            rowperpage: value_selectrowperpage,
            searchitem: value_search,
            offset: value_offset
        };
        $.ajax({
            url: '/usermanagement',
            type: 'POST',
            data: JSON.stringify(value),
            contentType: 'application/json',
            success: function (data) {
                $('#container').html(data);
                $('#selectshortbyitem').val(value_selectshortbyitem);
                $('#selectshortby').val(value_selectshortby);
                $('#selectrowperpage').val(value_selectrowperpage);
                $('#searchresult').attr('style', 'display:none;');
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $('#selectshortbyitem').on("change", function () {
        var value_selectshortbyitem = $('#selectshortbyitem').val();
        var value_selectshortby = $('#selectshortby').val();
        var value_selectrowperpage = $('#selectrowperpage').val();
        var value_search = $('#searchboxinput').val();
        var value_offset = 1;
        var value = {
            editreq: 0,
            shortbyitem: value_selectshortbyitem,
            shortby: value_selectshortby,
            rowperpage: value_selectrowperpage,
            searchitem: value_search,
            offset: value_offset
        };
        $.ajax({
            url: '/usermanagement',
            type: 'POST',
            data: JSON.stringify(value),
            contentType: 'application/json',
            success: function (data) {
                $('#container').html(data);
                $('#selectshortbyitem').val(value_selectshortbyitem);
                $('#selectshortby').val(value_selectshortby);
                $('#selectrowperpage').val(value_selectrowperpage);
                $('#searchresult').attr('style', 'display:none;');
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $('#selectshortby').on("change", function () {
        var value_selectshortbyitem = $('#selectshortbyitem').val();
        var value_selectshortby = $('#selectshortby').val();
        var value_selectrowperpage = $('#selectrowperpage').val();
        var value_search = $('#searchboxinput').val();
        var value_offset = 1;
        var value = {
            editreq: 0,
            shortbyitem: value_selectshortbyitem,
            shortby: value_selectshortby,
            rowperpage: value_selectrowperpage,
            searchitem: value_search,
            offset: value_offset
        };
        $.ajax({
            url: '/usermanagement',
            type: 'POST',
            data: JSON.stringify(value),
            contentType: 'application/json',
            success: function (data) {
                $('#container').html(data);
                $('#selectshortbyitem').val(value_selectshortbyitem);
                $('#selectshortby').val(value_selectshortby);
                $('#selectrowperpage').val(value_selectrowperpage);
                $('#searchresult').attr('style', 'display:none;');
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $('#searchboxinput').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var value_search = $('#searchboxinput').val();
        var value_selectrowperpage = $('#selectrowperpage').val();
        var value_offset = 1;
        var value = {
            editreq: 0,
            shortbyitem: "username",
            shortby: "ASC",
            rowperpage: value_selectrowperpage,
            searchitem: value_search,
            offset: value_offset
        };
        if (keycode == '13') {
            if (value_search.length > 0) {
                // $('#searchresult').removeAttr('style');
                // $('#searchlabel').text('Search Results for ' + value_search + " :")
                $.ajax({
                    url: '/usermanagement',
                    type: 'POST',
                    data: JSON.stringify(value),
                    contentType: 'application/json',
                    success: function (data) {
                        $('#container').html(data);
                        $('#selectshortbyitem').val("username");
                        $('#selectshortby').val("ASC")
                        $('#selectrowperpage').val(value_selectrowperpage);
                        $('#searchresult').removeAttr('style');
                        $('#searchboxinput').val(value_search);
                        $('#searchlabel').text('Search Results for ' + value_search + " :")
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });


            }
            if (value_search.length == 0) {
                $('#searchresult').attr('style', 'display:none;');
                $('#selectshortbyitem').val("username");
                $('#selectshortby').val("ASC").trigger("change");
            }
        }

    });


    $("#username-asc").click(function () {
        $('#selectshortbyitem').val("username");
        $('#selectshortby').val("ASC").trigger("change");
    });

    $("#username-desc").click(function () {
        $('#selectshortbyitem').val("username");
        $('#selectshortby').val("DESC").trigger("change");
    });

    $("#name-asc").click(function () {
        $('#selectshortbyitem').val("name");
        $('#selectshortby').val("ASC").trigger("change");
    });

    $("#name-desc").click(function () {
        $('#selectshortbyitem').val("name");
        $('#selectshortby').val("DESC").trigger("change");
    });

    $("#role-asc").click(function () {
        $('#selectshortbyitem').val("role");
        $('#selectshortby').val("ASC").trigger("change");
    });

    $("#role-desc").click(function () {
        $('#selectshortbyitem').val("role");
        $('#selectshortby').val("DESC").trigger("change");
    });

    $("#status-asc").click(function () {
        $('#selectshortbyitem').val("status");
        $('#selectshortby').val("ASC").trigger("change");
    });

    $("#status-desc").click(function () {
        $('#selectshortbyitem').val("status");
        $('#selectshortby').val("DESC").trigger("change");
    });

    $("#usermanagementtable").on("click", "a", function (e) {
        var value_selectshortbyitem = $('#selectshortbyitem').val();
        var value_selectshortby = $('#selectshortby').val();
        var value_selectrowperpage = $('#selectrowperpage').val();
        var value_search = $('#searchboxinput').val();
        if ($(e.target).hasClass('fas fa-arrow-up'))
            return;
        if ($(e.target).hasClass('fas fa-arrow-down'))
            return;
        var value = {
            editreq: 2,  //2 = unknown
            removeuser: 0,//0 : nonaktif 1 = aktif,
            activateuser: 0,
            resetpassword: 0,
            deactivateuser: 0,
            userlogin: '',
            username: ''
        };
        var row = $(this).closest("tr");
        var user_userlogin_value = row.find("td:eq(1)").first().text();
        var user_username_value = row.find("td:eq(2)").first().text();
        var el = $(this).attr('id');
        if (el === 'reset') {
            if (!confirm("Reset Password user : " + user_userlogin_value + " to \"pass\" ? ")) {
                return false;
            }
            var value = {
                editreq: 1,
                removeuser: 0, activateuser: 0,
                resetpassword: 1, deactivateuser: 0,
                userlogin: user_userlogin_value,
                username: user_username_value
            };
        }
        if (el === 'lockuser') {

            if (!confirm("Lock user : " + user_userlogin_value + " ? ")) {
                return false;
            }
            var value = {
                editreq: 1,
                removeuser: 0, activateuser: 0,
                resetpassword: 0, deactivateuser: 1,
                userlogin: user_userlogin_value,
                username: user_username_value
            };

        }
        if (el === 'unlockuser') {
            if (!confirm("Unlock user : " + user_userlogin_value + " ? ")) {
                return false;
            }
            var value = {
                editreq: 1,
                removeuser: 0, activateuser: 1,
                resetpassword: 0, deactivateuser: 0,
                userlogin: user_userlogin_value,
                username: user_username_value
            };

        }
        if (el === 'removeuser') {
            if (!confirm("Do you really want to delete user : " + user_userlogin_value + " ? ")) {
                return false;
            }
            var value = {
                editreq: 1,
                removeuser: 1, activateuser: 0,
                resetpassword: 0, deactivateuser: 0,
                userlogin: user_userlogin_value,
                username: user_username_value
            };

        }

        $.ajax({
            url: '/usermanagement',
            type: 'POST',
            data: JSON.stringify(value),
            contentType: 'application/json',
            success: function (data) {
                $('#container').html(data);
                $('#selectshortbyitem').val(value_selectshortbyitem);
                $('#selectshortby').val(value_selectshortby);
                $('#selectrowperpage').val(value_selectrowperpage);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    //pagination
    var value_startpagination = parseInt($('#currentpage').text());
    var value_counttotalrow =  parseInt($('#counttotalrow').text());
    var value_totalpagelbl = parseInt($('#totalpagelbl').text());

    $("#pagination_20").whjPaging({
        css: 'css-5',
        currPage: value_startpagination,
        totalSize: value_counttotalrow,
        totalPage: value_totalpagelbl,
        showPageNum: 5,
        previousPage: '<',
        nextPage: '>',
        isShowFL: true,
        isShowPageSizeOpt: false,
        isShowSkip: false,
        isShowRefresh: false,
        isShowTotalPage: false,
        isShowTotalSize: false,
        callBack: function (currPage, pageSize) {
            var value_selectrowperpage = $('#selectrowperpage').val();
            var value_offset = (value_selectrowperpage * (currPage - 1)) + 1;
            var value_selectshortbyitem = $('#selectshortbyitem').val();
            var value_selectshortby = $('#selectshortby').val();
            var value_search = $('#searchboxinput').val();
            var value = {
                editreq: 0,
                shortbyitem: value_selectshortbyitem,
                shortby: value_selectshortby,
                rowperpage: value_selectrowperpage,
                searchitem: value_search,
                offset: value_offset
            };

            $.ajax({
                url: '/usermanagement',
                type: 'POST',
                data: JSON.stringify(value),
                contentType: 'application/json',
                success: function (data) {
                    $('#container').html(data);
                    $('#selectshortbyitem').val(value_selectshortbyitem);
                    $('#selectshortby').val(value_selectshortby);
                    $('#selectrowperpage').val(value_selectrowperpage);
                    $('#searchboxinput').val(value_search);
                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
    });


});






